class BlogPost {
    constructor(data) {
        this.data = {};
        this.data.fefeId = data.fefeId;
        this.data.fefeDate = data.fefeDate;
        this.data.html = data.html;
    }
}
module.exports = BlogPost;