const https = require('https');

class NasaAPI {

    static async getRandomImage(term) {
        const imagesCount = await this.getImagesCount(term);
        let data = await this.getRandomImageData(imagesCount, term);
        data = await this.addImageUrl(data);
        return data;
    }

    static addImageUrl(data) {
        return new Promise((resolve, reject) => {
            https.get({
                hostname: 'images-api.nasa.gov',
                path: `/asset/${data.nasaId}`
            }, (resp) => {
                if(resp.statusCode > 400) {
                    reject(new Error('Failed to get data, status code: ' + resp.statusCode));
                }
                let jsonStr = '';
                resp.on('data', (chunk) => {
                    jsonStr += chunk;
                });
                resp.on('end', () => {
                    const jsonObj = JSON.parse(jsonStr);
                    for(let link of jsonObj.collection.items) {
                        if(link.href.endsWith('orig.jpg')) {
                            data.url = link.href
                            resolve(data);
                            return;
                        }
                    }
                    for(let link of jsonObj.collection.items) {
                        if(link.href.endsWith('large.jpg')) {
                            data.url = link.href
                            resolve(data);
                            return;
                        }
                    }
                    for(let link of jsonObj.collection.items) {
                        if(link.href.endsWith('medium.jpg')) {
                            data.url = link.href
                            resolve(data);
                            return;
                        }
                    }
                    for(let link of jsonObj.collection.items) {
                        if(link.href.endsWith('thumb.jpg')) {
                            data.url = link.href
                            resolve(data);
                            return;
                        }
                    }
                    reject(new Error('Faild to find any version of this Picture.'));
                });
            }).on('error', (error) => {
                reject(error);
            });
        });
    }
    
    static getRandomImageData(imagesCount, term) {
        const totalPages = Math.floor(imagesCount / 100);               // last page is lost here
        const randomPage = Math.floor(Math.random() * totalPages) + 1;
        console.log('page', randomPage);
        return new Promise((resolve, reject) => {
            https.get({
                hostname: 'images-api.nasa.gov',
                path: `/search?q=${term}&media_type=image&page=${randomPage}`
            }, (resp) => {
                if(resp.statusCode > 400) {
                    reject(new Error('Failed to get data, status code: ' + resp.statusCode));
                }
                let jsonStr = '';
                resp.on('data', (chunk) => {
                    jsonStr += chunk;
                });
                resp.on('end', () => {
                    const jsonObj = JSON.parse(jsonStr);
                    const randomIndex = Math.floor(Math.random() * 99);
                    let data = {};
                    console.log('index', randomIndex);
                    data.nasaId = jsonObj.collection.items[randomIndex].data[0].nasa_id;
                    data.title = jsonObj.collection.items[randomIndex].data[0].title;
                    data.description = jsonObj.collection.items[randomIndex].data[0].description.substring(0, 300) + ' ...';
                    data.metalocation = 'https://images-api.nasa.gov/metadata/' + data.nasaId;
                    resolve(data);
                });
            }).on('error', (error) => {
                reject(error);
            })
        });
    }

    static getImagesCount(term) {
        return new Promise((resolve, reject) => {
            https.get({
                hostname: 'images-api.nasa.gov',
                path: `/search?keywords=${term}&media_type=image`
            }, (resp) => {
                if (resp.statusCode > 400) {
                    reject(new Error('Failed to get data, status code: ' + resp.statusCode));
                }
                let jsonStr = '';
                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    jsonStr += chunk;
                });
                // The whole response has been received.
                resp.on('end', () => {
                    let jsonObj = JSON.parse(jsonStr);
                    const totalHits = jsonObj.collection.metadata.total_hits;
                    console.log('total hits', totalHits);
                    resolve(totalHits);
                });
            }).on('error', (error) => {
                reject(error)
            });
        });
    }

}

module.exports = NasaAPI;