require('dotenv').config();

const path = require('path');
const https = require('https');
const fs = require('fs');

const Datastore = require('nedb'); 
const Mastodon = require('mastodon-api');
const cheerio = require('cheerio');

const BlogPost = require('./BlogPost');
const NasaAPI = require('./NasaAPI');

const mastodonConfig = {
    client_key: process.env.CLIENT_KEY,
    client_secret: process.env.CLIENT_SECRET,
    access_token: process.env.ACCESS_TOKEN,
    timeout_ms: 60*1000,  // optional HTTP request timeout to apply to all requests.
    api_url: 'https://botsin.space/api/v1/'
  }

const M = new Mastodon(mastodonConfig);

console.log('Bot is starting...');

const query = 'galaxy';

NasaAPI.getRandomImage(query)
    .then((data) => {
        console.log(data);
        data.url = data.url.replace(/^(http)/,"https");
        const filename = data.url.split('/').pop();
        console.log(filename);

        tempDir = path.join(__dirname, 'tmp');
        tempFilePath = path.join(tempDir, filename);

        let file = fs.createWriteStream(tempFilePath);
        https.get(data.url, (response) => {
            response.pipe(file);
            file.on('finish', () => {
                file.close(() => {
                    console.log('file saved');

                    M.post('media', { file: fs.createReadStream(tempFilePath) }).then(resp => {
                        const id = resp.data.id;
                        M.post('statuses', {
                            status: `${data.title}\n\n${data.description}\n\nhttps://images.nasa.gov/details-${data.nasaId}.html`,
                            spoiler_text: "Learn something amazing about " + query + "s:",
                            media_ids: [id],
                            sensitive: false,
                            visibility: 'public'
                        }, (error, status) => {
                            if(error) {
                                console.error(error)
                            } else {
                                fs.readdir(tempDir, (err, files) => {
                                    if (err) throw err;
                                    for (const file of files) {
                                        fs.unlink(path.join(tempDir, file), err => {
                                            console.log('deleted ' + file);
                                            if (err) throw err;
                                        });
                                    }
                                    console.log('tooted!');
                                });
                            }
                        });
                    });
                });
            });
        }).on('error', (err) => { // Handle errors
            fs.unlink(tempFilePath); // Delete the file async. (But we don't check the result)
        });

    })
    .catch((error) => {
        console.log(error);
    });

const postsDatastoreFile = path.join(__dirname, 'data', 'fefe-posts.db');

let posts = new Datastore({
    filename: postsDatastoreFile,
    autoload: true
});

// 1. get html of actual and last month from blog.fefe.de
// 2. extract posts and create objects from it
// 3. calculate checksum for every post
// 4. check if post exists in database
// 5. compare checksum
// 6. if post not in database or checksum not equal toot post to mastodon

    /*
const params = {
    status: 'My first test post!'
}

M.post('statuses', params, (error, status) => {
    if(error) {
        console.error(error)
    } else {
        console.log(status);
    }
});
*/

/* let blogPosts = [];

https.get({
    hostname: 'blog.fefe.de',
    path: '/?mon=201810'
}, (resp) => {
    let html = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
        html += chunk;
    });
  
    // The whole response has been received. Print out the result.

    let data = {};

    resp.on('end', () => {
        const $ = cheerio.load(html);
        $('h3').each((i, h3) => {
            data.fefeDate = $(h3).text();
            $(h3).next().children().each((i, li) => {
                $(li).children().filter('a').each((i, a) => {
                    if(i === 0) {
                        data.fefeId = $(a).attr('href').substring(4);
                        $(a).addClass('remove');
                    }
                });
                $('.remove').remove();
                data.html = $(li).html().trim().replace(/(\r\n|\n|\r)/gm, '');
                blogPosts.push(new BlogPost(data));
            });
        });
    });
}).on("error", (err) => {
    console.log("Error: " + err.message);
});

for(let post of blogPosts) {
    console.log(post.fefeDate, post.fefeId);
}
 */