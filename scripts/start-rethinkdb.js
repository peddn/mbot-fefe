//const { exec } = require('child_process');
const { spawn } = require('child_process');

const command = 'rethinkdb';




const rethinkdb = spawn(command);

rethinkdb.stdout.on('data', (data) => {
  console.log(`${data}`);
});

rethinkdb.stderr.on('data', (data) => {
  console.log(`stderr: ${data}`);
});

rethinkdb.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});


/*
exec(command, (error, stdout, stderr) => {
  if (error) {
    console.error(`EXEC_ERROR: '${command}'`);
    console.error(error);
    console.log(`stdout: ${stdout}`);
    return;
  } else {
    if(stderr) {
        console.error(`COMMAND_ERROR: '${command}'`);
        console.log(stderr);
    } else {
        console.log(`OUTPUT: '${command}'`);
        console.log(stdout);
    }
  }
});
*/